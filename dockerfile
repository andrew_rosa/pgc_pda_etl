FROM rocker/tidyverse

RUN mkdir scripts
RUN mkdir .token

ADD ./01_process-forms-responses.R /scripts
ADD ./02_facilities-count.R /scripts
ADD ./etl_run.sh /scripts
ADD ./pgc-cwd-04277dc6cd4d.json /.token

RUN install2.r googledrive googlesheets4 lubridate

RUN chmod +x /scripts/01_process-forms-responses.R && \
    chmod +x /scripts/02_facilities-count.R && \
    chmod +x /scripts/etl_run.sh 
    
CMD ["./scripts/etl_run.sh"]